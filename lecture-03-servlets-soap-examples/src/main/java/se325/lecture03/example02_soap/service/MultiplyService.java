package se325.lecture03.example02_soap.service;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class MultiplyService {

    @WebMethod(operationName = "multiply")
    public int multiply(int arg0, int arg1) {
        return arg0 * arg1;
    }

}
