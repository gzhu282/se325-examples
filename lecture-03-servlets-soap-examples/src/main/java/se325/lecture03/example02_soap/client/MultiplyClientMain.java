package se325.lecture03.example02_soap.client;

import se325.lecture03.Keyboard;

public class MultiplyClientMain {

    public static void main(String[] args) {

        MultiplyServiceService serviceService = new MultiplyServiceService();
        MultiplyService multiplyService = serviceService.getMultiplyServicePort();

        int arg0 = Integer.parseInt(Keyboard.prompt("Enter first number:"));
        int arg1 = Integer.parseInt(Keyboard.prompt("Enter second number:"));

        int result = multiplyService.multiply(arg0, arg1);

        System.out.println("Result: " + result);

    }

}
