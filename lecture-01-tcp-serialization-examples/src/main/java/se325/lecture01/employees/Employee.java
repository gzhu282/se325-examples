package se325.lecture01.employees;

import java.io.Serializable;

public class Employee implements Serializable {

    protected String name;
    protected String ssn;
    protected Manager manager;

    public Employee(String name, String ssn, Manager manager) {
        this.name = name;
        this.ssn = ssn;
        this.manager = manager;

        if (manager != null) {
            manager.addEmployee(this);
        }
    }

    public Employee(String name, String ssn) {
        this(name, ssn, null);
    }

    public String getName() {
        return name;
    }

    public String getSsn() {
        return ssn;
    }

    public Manager getManager() {
        return manager;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        return new org.apache.commons.lang3.builder.EqualsBuilder()
                .append(name, employee.name)
                .append(ssn, employee.ssn)
                .append(manager, employee.manager)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
                .append(name)
                .append(ssn)
                .append(manager)
                .toHashCode();
    }
}
