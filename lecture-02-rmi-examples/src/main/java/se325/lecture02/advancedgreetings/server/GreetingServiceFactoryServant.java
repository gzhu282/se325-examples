package se325.lecture02.advancedgreetings.server;

import se325.lecture02.advancedgreetings.shared.GreetingService;
import se325.lecture02.advancedgreetings.shared.GreetingServiceFactory;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

public class GreetingServiceFactoryServant extends UnicastRemoteObject implements GreetingServiceFactory {

    private Map<String, GreetingService> services = new HashMap<>();

    public GreetingServiceFactoryServant() throws RemoteException {
        services.put("english", new EnglishGreetingServiceServant());
        services.put("maori", new MaoriGreetingServiceServant());
    }

    @Override
    public GreetingService getGreetingService(String language) throws RemoteException {
        return services.get(language);
    }
}
