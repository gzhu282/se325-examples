SOFTENG 325 - Software Architecture
==========
This course is concerned with supporting the development of complex software systems. Complex software tends to exhibit a combination of characteristics that may include largeness, concurrency, and physical distribution. Furthermore, non-trivial software systems are typically subject to a set of non-functional requirements. It is not sufficient for these systems to be functionally correct, but they must also satisfy non-functional requirements (constraints) governing security, scalability, availability, evolveability, etc.

Part One - Frameworks, Middleware and Web Services
----------
Developing modern applications necessitates use of application frameworks and middleware, which simplify development and integration of distributed information systems. Part One of the SE325 course draws on contemporary frameworks to explore key issues in service orientation and data access.

The topics & technologies covered in Part One are as follows:

- Lecture 01: [TCP, Java serialization](./lecture-01-tcp-serialization-examples).
- Lecture 02: [Java RMI](./lecture-02-rmi-examples).
- Lecture 03: [Java Servlets, REST vs SOAP](./lecture-03-servlets-soap-examples).
- Lecture 04: [REST services with JAX-RS](./lecture-04-jax-rs-examples).
- Lecture 05: [JSON with Jackson](./lecture-05-jackson-examples).
- Lecture 06: [Distributed data management techniques, introduction to ORM with JPA](./lecture-06-basic-jpa-examples).
- Lecture 07: Mapping classes & collections with JPA.
- Lecture 08: Mapping inheritance with JPA.
- Lecture 09: Managing transactions with JPA.
- Lecture 10: Fetch plans & strategies with JPA, overview of JPQL.
- Lecture 11: Asynchronous (publish-subscribe) web services with JAX-RS.